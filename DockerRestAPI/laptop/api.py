# Laptop Service

from flask import Flask, request
from flask_restful import Resource, Api
from flask_restful import reqparse
import csv
from pymongo import MongoClient
import os
from testToken import token_required, generate_auth_token
from password import hash_password, verify_password
import flask
from base64 import b64decode
import config

# Instantiate the app
app = Flask(__name__)
api = Api(app)

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb

CONFIG = config.configuration()

class listAll(Resource):
    def get(self):
        _items = db.tododb.find()
        items = [item for item in _items]
        flag = True
        opentime = []
        closetime = []
        for item in items:
            if flag:
                item['distance']
                flag = False
            else:
                opentime.append(item['open'])
                closetime.append(item['close'])
        return {"open": opentime, "close":closetime}
    
class listOpenOnly(Resource):
    def get(self):
        _items = db.tododb.find()
        items = [item for item in _items]
        flag = True
        opentime = []
        a = request.args.get('top')
        for item in items:
            if flag:
                item['distance']
                flag = False
            else:
                opentime.append(item['open'])
        new = []
        if a != None:
            for i in range(int(a)):
                new.append(opentime[i])
            return new

        return {"opentime" : opentime}


class listCloseOnly(Resource):
    def get(self):
        _items = db.tododb.find()
        items = [item for item in _items]
        flag = True
        closetime = []
        a = request.args.get('top')
        for item in items:
            if flag:
                item['distance']
                flag = False
            else:
                closetime.append(item['close'])
        new = []
        if a != None:
            for i in range(int(a)):
                new.append(closetime[i])
            return new
    
        return {"closetime" : closetime}

        
# Create routes
# Another way, without decorators
api.add_resource(listAll, '/listAll', '/listAll/json')
api.add_resource(listOpenOnly, '/listOpenOnly', '/listOpenOnly/json')
api.add_resource(listCloseOnly, '/listCloseOnly', '/listCloseOnly/json')

# reference: https://docs.python.org/3/library/csv.html

class csv_listAll(Resource):
    def get(self):
        _items = db.tododb.find()
        items = [item for item in _items]
        flag = True
        alltimes = []
    
        for item in items:
            if flag:
                item['distance']
                flag = False
            else:
                alltimes.append((item['open'] + ', ' + item['close']))
        with open('all.csv','w', newline = '')as csvfile:
            fieldnames = ['Alltimes']
            writer = csv.DictWriter(csvfile, fieldnames = fieldnames)
            
            writer.writeheader()
            for item in alltimes:
                writer.writerow({'Alltimes': item})

        return ['opentimes' + ',' + 'closetimes'] + alltimes
        

class csv_listOpenOnly(Resource):
    def get(self):
        _items = db.tododb.find()
        items = [item for item in _items]
        flag = True
        opentime = []
        a = request.args.get('top')
        for item in items:
            if flag:
                item['distance']
                flag = False
            else:
                opentime.append(item['open'])
        new = []
        if a != None:
            for i in range(int(a)):
                new.append(opentime[i+1])
            with open('open.csv','w', newline = '')as csvfile:
                fieldnames = ['opentimes']
                writer = csv.DictWriter(csvfile, fieldnames = fieldnames)
                writer.writeheader()
                for item in new:
                    writer.writerow({'opentimes': item})    
            return ['opentimes'] + new
  
        with open('open.csv','w', newline = '')as csvfile:
            fieldnames = ['opentimes']
            writer = csv.DictWriter(csvfile, fieldnames = fieldnames)
            
            writer.writeheader()
            for item in opentime:
                writer.writerow({'opentimes': item})

        return ['opentimes'] + opentime

class csv_listCloseOnly(Resource):
    def get(self):
        _items = db.tododb.find()
        items = [item for item in _items]
        flag = True
        closetime = []
        a = request.args.get('top')
        for item in items:
            if flag:
                item['distance']
                flag = False
            else:
                closetime.append(item['close'])
        new = []
        if a != None:
            for i in range(int(a)):
                new.append(closetime[i+1])
            with open('close.csv','w', newline = '')as csvfile:
                fieldnames = ['closetimes']
                writer = csv.DictWriter(csvfile, fieldnames = fieldnames)
                writer.writeheader()
                for item in new:
                    writer.writerow({'closetimes': item})
            return ['closetimes'] + new

        with open('close.csv','w', newline = '')as csvfile:
            fieldnames = ['closetimes']
            writer = csv.DictWriter(csvfile, fieldnames = fieldnames)
            
            writer.writeheader()
            for item in closetime:
                writer.writerow({'closetimes': item})
    
        return ['closetimes'] + closetime

api.add_resource(csv_listAll, '/listAll/csv')
api.add_resource(csv_listOpenOnly, '/listOpenOnly/csv')
api.add_resource(csv_listCloseOnly, '/listCloseOnly/csv')

@app.route("/api/register", methods=["POST"])
def register():
    user_name = request.form.get("user_name")
    password = request.form.get("password")
    if user_name == None or password == None:
        return flask.jsonify({"Incorrect": "need username or passward"}), 400
    if db.user.find_one({"user_name": user_name}) != None:
        return flask.jsonify({"Incorrect": "only one username"}), 400
    Password = hash_password(password)
    db.user.insert_one({"user_name": user_name, "password": Password})
    return flask.jsonify({"user_name": user_name}), 201

@app.route("/api/token")
def token():
    header = request.headers.get("Authorization")
    space, message = header.split(" ", 1)
    user_ = b64decode(message)
    user_name, password = user_.decode().split(":", 1)
    user = db.user.find_one({"user_name": user_name})
    if user == None:
        return flask.jsonify({"Incorrect": "can not find user"}), 401
    if not verify_password(password, user['password']):
        return flask.jsonify({"Incorrect": "passward format wrong"}), 401
    token = generate_auth_token(expiration=CONFIG.TIME)
    return flask.jsonify({"duration": CONFIG.TIME, "token": token.decode()}), 200


# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)

