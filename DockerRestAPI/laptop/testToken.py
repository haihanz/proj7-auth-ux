from itsdangerous import (TimedJSONWebSignatureSerializer \
                                  as Serializer, BadSignature, \
                                  SignatureExpired)
import time
import config

from functools import wraps
from base64 import b64decode
from flask import request
import flask


# initialization
# app = Flask(__name__)
# app.config['SECRET_KEY'] = 'the quick brown fox jumps over the lazy dog'

CONFIG = config.configuration()

def generate_auth_token(expiration=600, key=CONFIG.SEC_KEY):
   # s = Serializer(app.config['SECRET_KEY'], expires_in=expiration)
   s = Serializer(key, expires_in=expiration)
   # pass index of user
   return s.dumps({'id': 1})

def verify_auth_token(token, key=CONFIG.SEC_KEY):
    s = Serializer(key)
    try:
        data = s.loads(token)
    except SignatureExpired:
        return False    # valid token, but expired
    except BadSignature:
        return False    # invalid token
    return True

def token_required(f):
  @wraps(f)
  def decorated_function(*args, **kwargs):
    header = request.headers.get("Authorization")
    space, message = header.split(" ", 1)
    user = b64decode(message)
    take, psw = user.decode().split(":", 1)
    if verify_auth_token(take):
      return f(*args, **kwargs)
    else:
      return {"incorrect":"not working!"}, 401
  return decorated_function